# Briar

Briar is an secure messaginng and blogging platform that jumps from device to device(P2P).
Online it works over Tor network so you have anonimity and works offline also over wifi and bluetooth meshnets.
There is no servers involved, no third parties only members of communication(chat, blog, forum etc.) Have access to
data and metadata, it's fully peer-to-peer.

It's available on android and desktop, altough desktop version is currently limited in a sense that it only supports chat.

# Android installation

First option is to download `apk` file from their website, https://briarproject.org/apk.

Second option is from F-Droid ,first install [F-Droid]()https://f-droid.org/, an open source app store. In there search for "briar", download and install.
Here are more detailed instructions. https://briarproject.org/installing-briar-via-f-droid/

# Desktop installation

Briar for desktop is currenlty limited only to text chat. Download a version that fits you operating system from [here](https://briarproject.org/download-briar-desktop/).


# Identity and contacts

When you first run briar it will ask you for a password. Don't forget it since data cannot be recovered, and briar is in locked state when not used.
To add your friends you will need your code and they will need yours, you exchange it and select "Add contact at a distance" option, insert code there and you 
are connected. Other option is to add people via bluetooth if you are close in person or over wifi if you are on the same network. 


# Groups, forums and blogs

Briar, besides chat, supports group chat where only creator of chat can add people. It supports forums where everyone can add people and it supports blogs which is like your personal feed
where your other contacts can reply. Naming is a bit confusing so you can look at a forum as an open group and blog section as timeline where you see what your contacts are writting.


