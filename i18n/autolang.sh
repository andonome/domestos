#!/bin/bash
# This script attempts to automatically generate a language file.
# It produces ugly results with strange grammar, but it might save a translator some time.

[ -z "$1" ] && echo 'Start this script with a language code, e.g. "./autolang.sh el"'

cat en.toml | while read -r line; do
	echo "$line" | grep '=' -q  && \
	old="$(echo "$line" | cut -d= -f2)" && \
	new="$(echo "$old" | trans en:$1 -b -e google)" && \
	echo "$line" | sed "s/$old/ $new/" >> $1.toml || \
	echo "$line" >> $1.toml
done
